import React from 'react';
import './InputSearch.scss';

interface IProps {
  value: string,
  placeholder?: string,
  onChange(value: string): void
}

const InputSearch: React.FC<IProps> = (props) => {
  const handlerChange = (event: React.FormEvent<HTMLInputElement>): void => {
    const { value } = event.currentTarget;
    props.onChange(value);
  };

  const resetValue = (): void => {
    props.onChange('');
  };

  return (
    <div className="search-form">
      <input
        value={props.value}
        onChange={handlerChange}
        type="text"
        placeholder={props.placeholder}
      />

      <div className="search-form__icon">
        {!props.value.length ?
          (
            <div className="search-form__icon-zoom" />
          ) : (
            <button
              className="search-form__reset"
              onClick={resetValue}
            />
          )
        }
      </div>
    </div>
  );
};

export default InputSearch;
