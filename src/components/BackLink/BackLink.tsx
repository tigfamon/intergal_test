import React from 'react';
import './BackLink.scss';
import { IBackLink } from '../../types';

import { connect } from 'react-redux';
import { AppState } from '../../store';
import { Link } from 'react-router-dom';

interface IProps {
  value: IBackLink
}

const BackLink: React.FC<IProps> = (props) => {
  const { pathname, name } = props.value;
  return (
    <Link to={{pathname}} className="back-link">
      {name}
    </Link>
  );
};

const mapStateToProps = (state: AppState) => ({
  value: state.backLink
});

export default connect(mapStateToProps)(BackLink);
