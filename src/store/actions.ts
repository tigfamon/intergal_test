import {
  IFilms, IFilmsFilter, IBackLink,
  SET_FILMS, SET_FILTER, SET_BACKLINK,
  StoreActionsType
} from '../types';

export function setFilms(data: IFilms): StoreActionsType {
  return {
    type: SET_FILMS,
    data
  };
}

export function setFilter(data: IFilmsFilter): StoreActionsType {
  return {
    type: SET_FILTER,
    data
  };
}

export function setBackLink(data: IBackLink) {
  return {
    type: SET_BACKLINK,
    data
  };
}
