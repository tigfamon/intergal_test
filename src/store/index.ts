import {
  SET_FILMS, SET_FILTER, SET_BACKLINK,
  StoreActionsType, IStoreStateType } from '../types';

const initialState: IStoreStateType = {
  films: {
    list: [],
    filteredList: []
  },
  filter: {
    searchByName: '',
    sortByDate: ''
  },
  backLink: {
    name: '',
    pathname: ''
  }
};

export type AppState = ReturnType<typeof rootReducer>;

export default function rootReducer(state = initialState, action: StoreActionsType) {
  switch (action.type) {
    case SET_FILMS:
      return {
        ...state, films: action.data
      };
    case SET_FILTER:
      return {
        ...state, filter: action.data
      };
    case SET_BACKLINK:
      return {
        ...state, backLink: action.data
      };
    default:
      return state;
  }
}
