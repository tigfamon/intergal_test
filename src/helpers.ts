/**
 * @desc Получение рандомного числа от min до max
 * @param {Number} min минимум
 * @param {Number} max максимум
 * @return {Number}
 */
const getRandomNumber = (min: number, max: number): number => {
  const rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
};

/**
 * @desc Преобразование даты в формат строки DD.MM.YYYY
 * @param {Date} value
 * @return {String}
 */
const setNumericDate = (value: Date): string => {
  if (value) {
    const date = new Date(value);
    return date.toLocaleString('ru-RU', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric'
    });
  }
  return '__.__.____';
};

/**
 * @desc Перевод пола на русский
 * @param {String} value - значение
 * @return {String}
 */
const translateGender = (value: string): string => {
  const genders: {[key: string]: string } = {
    male: 'Мужчина',
    female: 'Женщина'
  };

  return genders[value] || 'Пол неопределен';
};

export {
  getRandomNumber, setNumericDate, translateGender
};
