import axios from './axios';
import { IFilm } from '../types';

type Films = {
  results: IFilm[]
};

export default {
  /**
   * @desc Получение списка фильмов
   */
  getFilms: () => axios.get<Films>('/films/').then(res => res.data),
  /**
   * @desc Получение фильма по id
   * @param {Number} episode_id - id фильма
   */
  getFilmByID: (episode_id: number) => axios.get<IFilm>(`/films/${episode_id}`).then(res => res.data)
};
