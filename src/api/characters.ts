import axios from './axios';
import { ICharacter } from '../types';

export default {
  /**
   * @desc Получение актера по id
   * @param {Number} id - id актера
   */
  getCharacterByID: (id: number) => axios.get<ICharacter>(`/people/${id}`).then(res => res.data)
};