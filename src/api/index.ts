import films from './films';
import characters from './characters';

export default {
  films, characters
};
