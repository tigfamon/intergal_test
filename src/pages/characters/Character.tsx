import React from 'react';
import './Character.scss';
import { ICharacter } from '../../types';
import apiCharacters from '../../api/characters';

import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { setBackLink } from '../../store/actions';
import { getRandomNumber, translateGender } from '../../helpers';

import BackLink from '../../components/BackLink/BackLink';

// import images for character photo
import photo1 from '../../assets/images/character_1.png';
import photo2 from '../../assets/images/character_2.png';
import photo3 from '../../assets/images/character_3.png';

interface IMatchParams {
  id: string,
  filmID: string
}

interface IProps extends RouteComponentProps<IMatchParams> {
  setBackLink: typeof setBackLink
}

interface IState {
  character: ICharacter,
  isLoaded: boolean
}

class Character extends React.Component<IProps, IState> {
  public state: IState = {
    character: {
      name: '',
      gender: '',
      mass: '',
      height: '',
      birth_year: ''
    },
    isLoaded: false
  };

  public getCharacter = async () => {
    const id = this.props.match.params.id;
    const data = await apiCharacters.getCharacterByID(Number(id));

    const images: string[] = [photo1, photo2, photo3];
    this.setState({
      character: Object.assign({}, data, {
        img: images[getRandomNumber(0, images.length - 1)]
      })
    });
  };

  async componentDidMount(): Promise<void> {
    this.props.setBackLink({
      name: 'Назад к фильму',
      pathname: `/${this.props.match.params.filmID}`
    });
    await this.getCharacter();
    this.setState({isLoaded: true});
  }

  public render() {
    const { character, isLoaded } = this.state;

    return (
      isLoaded ?
        (
          <>
            <BackLink />
            <div className="character-info">
              <div
                className="character-info__img"
                style={{backgroundImage: `url(${character.img})`}}
              />
              <div className="character-info__text">
                <h3 className="character-info__title">{character.name}</h3>
                <div className="text-description character-info__description">
                  <p>
                    {character.gender === 'unknown' ? 'Пол неизвестен, ' : translateGender(character.gender)},
                    {character.birth_year === 'unknown' ? ' Дата рождения неизвестна' : ` год рождения: ${character.birth_year}` }
                  </p>
                  <p>
                    Вес: {character.mass === 'unknown' ? `неизвестен, ` : `${character.mass}кг, `}
                    Рост: {character.height === 'unknown' ? ` неизвестен, ` : `${character.height}см`}
                  </p>
                </div>
              </div>

            </div>
          </>
        ) : null
    );
  }
}


export default withRouter(connect(null, { setBackLink })(Character));