import React from 'react';
import InputSearch from '../../../components/InputSearch/InputSearch';

interface Props {
  searchValue: string,
  sortByDateValue: string,
  onSearch(value: string): void,
  onSortByDate(): void
}

const FilmsFilter: React.FC<Props> = (props) => {
  return(
    <div className="films__filter">
      <InputSearch
        value={props.searchValue}
        onChange={(value) => props.onSearch(value)}
        placeholder={'Поиск по названию фильма'}
      />

      <div className="films__filter-sort-date">
        <button
          className={`btn-sort btn-sort__${props.sortByDateValue}`}
          onClick={props.onSortByDate}
        >
          Дата премьеры</button>
      </div>
    </div>
  );
};

export default FilmsFilter;
