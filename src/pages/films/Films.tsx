import React from 'react';
import './Films.scss';
import { IFilms, IFilmsFilter } from '../../types';
import apiFilms from '../../api/films';

import { getRandomNumber, setNumericDate } from '../../helpers';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { AppState } from '../../store';
import { setFilms as setFilmsStore, setFilter as setFilterStore } from '../../store/actions';

import FilmsFilter from './components/FilmsFilter';

// import images for films
import film1 from '../../assets/images/film_1.jpg';
import film2 from '../../assets/images/film_2.jpg';
import film3 from '../../assets/images/film_3.jpg';
import film4 from '../../assets/images/film_4.jpg';

interface IProps {
  films: IFilms,
  filter: IFilmsFilter,
  setFilmsStore: typeof setFilmsStore,
  setFilterStore: typeof setFilterStore
}

class Films extends React.Component<IProps, any> {

  public getFilms = async () => {
    const data = await apiFilms.getFilms();

    // set images for films
    const images: string[] = [film1, film2, film3, film4];
    const items = data.results.map(el => {
      return Object.assign(el, {}, {
        img: images[getRandomNumber(0, images.length - 1)]
      });
    });

    this.props.setFilmsStore({ list: items, filteredList: items });
  };

  public searchHandler = (value: string) => {
    const { sortByDate } = this.props.filter;
    const { list } = this.props.films;

    this.props.setFilterStore({
      searchByName: value,
      sortByDate
    });

    this.props.setFilmsStore({
      list,
      filteredList: list.filter(film => {
        const title = film.title.toLowerCase();
        return title.includes(value.toLowerCase());
      })
    });
  };

  public sortByDateHandler = () => {
    const { searchByName, sortByDate } = this.props.filter;
    const { list } = this.props.films;
    let newValue: string = '';

    if (!sortByDate) {
      newValue = 'desc';
      this.props.setFilmsStore({
        list,
        filteredList: list.sort((a, b) => {
          return +new Date(b.release_date) - +new Date(a.release_date);
        })
      });

    } else if (sortByDate === 'desc') {
      newValue = 'asc';
      this.props.setFilmsStore({
        list,
        filteredList: list.sort((a, b) => {
          return +new Date(a.release_date) - +new Date(b.release_date);
        })
      });

    } else if (sortByDate === 'asc') {
      newValue = '';
    }

    this.searchHandler(searchByName);

    this.props.setFilterStore({
      searchByName,
      sortByDate: newValue
    });
  };

  componentDidMount(): void {
    if (!this.props.films.list.length) {
      this.getFilms();
    }
  };

  public render() {
    const { films, filter } = this.props;
    return (
      <>
        <FilmsFilter
          searchValue={filter.searchByName}
          sortByDateValue={filter.sortByDate}
          onSortByDate={this.sortByDateHandler}
          onSearch={(value) => this.searchHandler(value)}
        />
        <div className="films">
          {films.filteredList.map((film, index) => {
            // получаем id фильма из ссылки
            const id = film.url.match(/\d+/)![0];

            return(
              <Link to={{pathname: `/${id}`}}
                    key={index}
                    className="films__cart"
              >
                <div
                  className="films__cart-img"
                  style={{backgroundImage: `url(${film.img})`}}
                />
                <h3 className="text-title">{film.title}</h3>
                <p className="text-description">{setNumericDate(film.release_date)}</p>
              </Link>
            );
          })}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  films: state.films,
  filter: state.filter
});

export default withRouter(connect(mapStateToProps, {
  setFilmsStore, setFilterStore
})(Films));
