import React from 'react';
import './Film.scss';
import { IFilm } from '../../types';
import apiFilms from '../../api/films';

import { connect } from 'react-redux';
import { withRouter, Link, RouteComponentProps } from 'react-router-dom';
import { setNumericDate } from '../../helpers';
import { setBackLink } from '../../store/actions';
import BackLink from '../../components/BackLink/BackLink';

interface IMatchParams {
  id: string
}

interface IProps extends RouteComponentProps<IMatchParams> {
  setBackLink: typeof setBackLink
}

interface IState {
  film: IFilm,
  isLoaded: boolean
}

class Film extends React.Component<IProps, IState> {
  public state: IState = {
    film: {
      episode_id: 0,
      title: '',
      release_date: new Date(),
      characters: [],
      director: '',
      url: ''
    },
    isLoaded: false
  };

  public getFilm = async () => {
    const id = this.props.match.params.id;
    const data = await apiFilms.getFilmByID(Number(id));
    this.setState({
      film: data
    });
  };

  async componentDidMount(): Promise<void> {
    this.props.setBackLink({
      name: 'К списку фильмов',
      pathname: '/'
    });
    await this.getFilm();
    this.setState({isLoaded: true});
  }

  public render() {
    const { film, isLoaded } = this.state;
    const characters = film.characters.map((el, index) => {
      // получаем id актера из ссылки
      const id = el.match(/\d+/)![0];

      return (
        <Link
          to={{pathname: `/${this.props.match.params.id}/characters/${id}`}}
          key={index}
        >
          { index + 1 !== film.characters.length ? (
            <span>Актёр - {index}, </span>
          ) : (
            <span>Актёр - {index}.</span>
          ) }
        </Link>
      );

    });

    return (
      isLoaded ?
        (
          <>
            <BackLink />
            <div className="film-info">
              <h3 className="film-info__title">{film.title}</h3>
              <div className="film-info__description text-description">
                <p>Режиссер: {film.director}</p>
                <p>Дата выхода: {setNumericDate(film.release_date)}</p>
                <p>В главных ролях: {characters}</p>
              </div>
            </div>
          </>
        ) : null
    );
  }
}


export default withRouter(connect(null, { setBackLink })(Film));
