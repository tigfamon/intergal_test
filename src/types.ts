export interface IFilm {
  episode_id: number,
  title: string,
  release_date: Date,
  characters: string[]
  img?: string,
  director?: string,
  url: string
}

export interface ICharacter {
  name: string,
  img?: string,
  gender: string,
  mass: string,
  height: string,
  birth_year: string
}

export interface IFilms {
  list: IFilm[],
  filteredList: IFilm[],
}

export interface IFilmsFilter {
  searchByName: string,
  sortByDate: string
}

export interface IBackLink {
  name: string,
  pathname: string
}

// store types
export interface IStoreStateType {
  films: IFilms,
  filter: IFilmsFilter,
  backLink: IBackLink
}

export const SET_FILMS = 'SET_FILMS';
export const SET_FILTER = 'SET_FILTER';
export const SET_BACKLINK = 'SET_BACKLINK';

interface ISetFilmsAction {
  type: typeof SET_FILMS,
  data: IFilms
}

interface ISetFilterAction {
  type: typeof SET_FILTER,
  data: IFilmsFilter
}

interface ISetBackLink {
  type: typeof SET_BACKLINK,
  data: IBackLink
}

export type StoreActionsType = ISetFilmsAction | ISetFilterAction | ISetBackLink;
