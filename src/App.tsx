import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// import Pages
import Films from './pages/films/Films';
import Film from './pages/film/Film';
import Character from './pages/characters/Character';

class App extends React.Component {
  public render() {
    const routes = [
      {
        path: '/',
        exact: true,
        component: Films
      },
      {
        path: '/:id',
        exact: true,
        component: Film
      },
      {
        path: '/:filmID/characters/:id',
        exact: true,
        component: Character
      }
    ];

    return (
      <div className="container">
        <Router>
          <Switch>
            {routes.map((route, index) => (
              <Route
                key={index}
                exact={route.exact}
                path={route.path}
                component={route.component}
              />
            ))}
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
